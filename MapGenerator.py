# Python Classes
import time
import string
import random

# PyGame Classes
import pygame
from pygame.locals import *

class MapGenerator:
    def __init__(self):
        self.screen = 0
        global land,chestOpen,goblinDead
        land = pygame.image.load("tiles/land.png").convert()
        goblinDead = pygame.image.load("tiles/goblinDead.png").convert()
        goblinDead.set_colorkey(pygame.Color("blue"))
        chestOpen = pygame.image.load("tiles/chestOpen.png").convert()


    # A map generator that will eitehr paint a water or land tile.
    # The values in the matrix are 0 for water and 1 for land.
    def getTile(self, x,y,screen):
        tresureRNG = random.randint(10,110)
        RNG = random.randint(1, 10)

        # create a 1 by 1 tile of land at spawn point
        if ((x < 1) and (y < 1)):
            return 0
        # create a water tile, with a value of 1
        if(RNG < 2):
            return 1
        elif((RNG >= 2) and (RNG < 3)):
            return 2
        # create a land tile, with a value of 0
        if (tresureRNG == 77):
            return 3
        else:
            return 0

    # Creating the map matrix
    def generate(self, screen):
        global map_array
        self.screen = screen
        (width, height) = (11,8)
        map_array = [[self.getTile(x,y, screen) for y in range(height)] for x in range(width)]
        return  map_array

    def update(self, xPos, yPos, screen):
        screen.blit(land, (xPos, yPos))

    def openTreasure(self, xPos, yPos, screen):
        screen.blit(chestOpen, ((xPos*60), (yPos*60)))

    def killGoblin(self, xPos, yPos, screen):
        print("xpos,ypos = " + str(xPos) + "," + str(yPos))
        map_array[xPos][yPos] = 6
        screen.blit(land, ((xPos*60), (yPos*60)))
        screen.blit(goblinDead, ((xPos*60), (yPos*60)))

    def paintMap(self, map_array, screen):
        waterWhole = pygame.image.load("tiles/waterWhole.png").convert()
        chest = pygame.image.load("tiles/chest.png").convert()
        tree = pygame.image.load("tiles/tree.png").convert()
        water = pygame.image.load("tiles/water.png").convert()

        #print "map_arry = " + str(len(map_array))
        #print "map_arry[1] = " + str(len(map_array[1]))
        count = 0
        for i in range(len(map_array)):
            for j in range(len(map_array[1])):
                count += 1
                tileID = map_array[i][j]
                #paint land
                if (tileID == 0):
                    screen.blit(land,((i*60),(j*60)))
                #paint waters
                if (tileID == 1):
                    screen.blit(waterWhole,((i*60),(j*60)))

                #paint trees
                if (tileID == 2):
                    screen.blit(tree,((i*60),(j*60)))
                #paint chest
                if (tileID == 3):
                    screen.blit(chest,((i*60),(j*60)))

