import pygame
from pygame.locals import *

from MapGenerator import MapGenerator

class Player(pygame.sprite.Sprite):

    def __init__(self, screen):
        global man, manRect, currentPos, previousPos
        currentPos = [0,0]
        previousPos = [0,0]

        man = pygame.image.load("tiles/man.png").convert()
        man.set_colorkey(pygame.Color("blue"))
        manRect = man.get_rect()
        screen.blit(man, manRect)

    def blitMan(self, screen):
        screen.blit(man, manRect)

    def move(self, map_array, MapGen, screen, size, key, enemies):

        # TODO Create a check for chests
        # TODO Create a check for enemy
        # if user enters DOWN, move character down
        if key == pygame.K_DOWN:
            if ((manRect.top + 120) <= size[1]):
                #Check for treasure
                if (map_array[(currentPos[0])][(currentPos[1]+1)] == 3):
                    MapGen.openTreasure(currentPos[0],(currentPos[1]+1),screen)

                #Check for enemy
                if (map_array[(currentPos[0])][(currentPos[1]+1)] == 5):
                    print("Check enemy down")
                    MapGen.killGoblin(currentPos[0],(currentPos[1]+1),screen)
                    enemies.killGoblin()

                if (        (map_array[(currentPos[0])][(currentPos[1]+1)] != 1) \
                        and (map_array[(currentPos[0])][(currentPos[1]+1)] != 2) \
                        and (map_array[(currentPos[0])][(currentPos[1]+1)] != 3) \
                        and (map_array[(currentPos[0])][(currentPos[1]+1)] != 5)):
                    previousPos[1] = currentPos[1]
                    previousPos[0] = currentPos[0]
                    currentPos[1] += 1
                    manRect.top += 60
        # if user enters UP, move character up
        if key == pygame.K_UP:
            if (manRect.top - 60) >= 0:
                #Check for treasure
                if (map_array[(currentPos[0])][(currentPos[1]-1)] == 3):
                    MapGen.openTreasure(currentPos[0],(currentPos[1]-1),screen)

                #Check for enemy
                if (map_array[(currentPos[0])][(currentPos[1]-1)] == 5):
                    print("Check enemy up")
                    MapGen.killGoblin(currentPos[0],(currentPos[1]-1),screen)
                    enemies.killGoblin()

                if (        (map_array[(currentPos[0])][(currentPos[1]-1)] != 1) \
                        and (map_array[(currentPos[0])][(currentPos[1]-1)] != 2) \
                        and (map_array[(currentPos[0])][(currentPos[1]-1)] != 3) \
                        and (map_array[(currentPos[0])][(currentPos[1]-1)] != 5)):
                    previousPos[1] = currentPos[1]
                    previousPos[0] = currentPos[0]
                    currentPos[1] -= 1
                    manRect.top -= 60
        # if user enters LEFT, move character left
        if key == pygame.K_LEFT:
            if (manRect.left - 60) >= 0:
                #Check for treasure
                if (map_array[(currentPos[0]-1)][(currentPos[1])] == 3):
                    MapGen.openTreasure((currentPos[0]-1),(currentPos[1]),screen)

                #Check for enemy
                if (map_array[(currentPos[0]-1)][(currentPos[1])] == 5):
                    print("check enemy left")
                    MapGen.killGoblin((currentPos[0]-1),(currentPos[1]),screen)
                    enemies.killGoblin()

                if (        (map_array[(currentPos[0]-1)][currentPos[1]] != 1) \
                        and (map_array[(currentPos[0]-1)][currentPos[1]] != 2) \
                        and (map_array[(currentPos[0]-1)][currentPos[1]] != 3) \
                        and (map_array[(currentPos[0]-1)][currentPos[1]] != 5) \
                        ):
                    previousPos[1] = currentPos[1]
                    previousPos[0] = currentPos[0]
                    currentPos[0] -= 1
                    manRect.left -= 60

        # if user enters RIGHT, move character right
        if key == pygame.K_RIGHT:
            if (manRect.left + 120) <= size[0]:
                #Check for treasure
                if (map_array[(currentPos[0]+1)][(currentPos[1])] == 3):
                    MapGen.openTreasure((currentPos[0]+1),(currentPos[1]),screen)

                #Check for enemy
                if (map_array[(currentPos[0]+1)][(currentPos[1])] == 5):
                    print("check enemy right")
                    MapGen.killGoblin((currentPos[0]+1),(currentPos[1]),screen)
                    enemies.killGoblin()

                if (        (map_array[(currentPos[0]+1)][currentPos[1]] != 1) \
                        and (map_array[(currentPos[0]+1)][currentPos[1]] != 2) \
                        and (map_array[(currentPos[0]+1)][currentPos[1]] != 3) \
                        and (map_array[(currentPos[0]+1)][currentPos[1]] != 5)):
                    previousPos[1] = currentPos[1]
                    previousPos[0] = currentPos[0]
                    currentPos[0] += 1
                    manRect.left += 60

        map_array[previousPos[0]][previousPos[1]] = 0
        map_array[currentPos[0]][currentPos[1]] = 4

        screen.blit(man, manRect)

        #update map with a land tile
        if ((map_array[previousPos[0]][previousPos[1]] == 0) \
                and (map_array[previousPos[0]][previousPos[1]] != 6)):
            MapGen.update((previousPos[0]*60),(previousPos[1]*60), screen)

