import pygame
import random
from pygame.locals import *

from MapGenerator import MapGenerator

class Enemy(pygame.sprite.Sprite):

    def __init__(self):
        global goblin, goblinRect, dead
        goblin = pygame.image.load("tiles/goblin.png").convert()
        goblinRect = goblin.get_rect()
        goblin.set_colorkey(pygame.Color("blue"))
        print("GoblinRect init: " + str(goblinRect))
        dead = False

    def checkPos(self, map_arrayIn, screen):
        global enemyCurrentPos, enemyPreviousPos
        map_array = map_arrayIn
        RNGposX = random.randint(1,7)
        RNGposY = random.randint(1,7)
        print("RNGposX: " + str(RNGposX))
        print("RNGposY: " + str(RNGposY))
        print("GoblinRect check: " + str(goblinRect))
        if (((map_array[RNGposX][RNGposY]) != 1) and (map_array[RNGposX][RNGposY] != 2)):
            enemyCurrentPos = [RNGposX,RNGposY]
            enemyPreviousPos = [RNGposX,RNGposY]
            goblinRect.top = 60*RNGposY
            goblinRect.left = 60*RNGposX
            print("GoblinRect check2: " + str(goblinRect))
        else:
            self.checkPos(map_array, screen)

        screen.blit(goblin, goblinRect)

    def blitEnemy(self, screen):
        screen.blit(goblin, goblinRect)

    def move(self, map_array, MapGen, screen, size):
        if (dead == False):
            RNG = random.randint(0,3)
            #print("Enemy current: " + str(enemyCurrentPos))
            #print("Enemy previous: " + str(enemyPreviousPos))
            #print("RNG: " + str(RNG))
            #print("GoblinRect move: " + str(goblinRect))

            # if user enters DOWN, move character down and repaint background
            if RNG == 0:
                if ((goblinRect.top + 120) <= size[1]):
                    if (        (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]+1)] != 1) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]+1)] != 2) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]+1)] != 3) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]+1)] != 4)):
                        enemyPreviousPos[1] = enemyCurrentPos[1]
                        enemyPreviousPos[0] = enemyCurrentPos[0]
                        enemyCurrentPos[1] += 1
                        goblinRect.top += 60
            # if user enters UP, move character down and repaint background
            if RNG == 1:
                if (goblinRect.top - 60) >= 0:
                    if (        (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]-1)] != 1) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]-1)] != 2) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]-1)] != 3) \
                            and (map_array[(enemyCurrentPos[0])][(enemyCurrentPos[1]-1)] != 4)):
                        enemyPreviousPos[1] = enemyCurrentPos[1]
                        enemyPreviousPos[0] = enemyCurrentPos[0]
                        enemyCurrentPos[1] -= 1
                        goblinRect.top -= 60
            # if user enters LEFT, move character down and repaint background
            if RNG == 2:
                if (goblinRect.left - 60) >= 0:
                    if (        (map_array[(enemyCurrentPos[0]-1)][enemyCurrentPos[1]] != 1) \
                            and (map_array[(enemyCurrentPos[0]-1)][enemyCurrentPos[1]] != 2) \
                            and (map_array[(enemyCurrentPos[0]-1)][enemyCurrentPos[1]] != 3) \
                            and (map_array[(enemyCurrentPos[0]-1)][enemyCurrentPos[1]] != 4)):
                        enemyPreviousPos[1] = enemyCurrentPos[1]
                        enemyPreviousPos[0] = enemyCurrentPos[0]
                        enemyCurrentPos[0] -= 1
                        goblinRect.left -= 60
            # if user enters RIGHT, move character down and repaint background
            if RNG == 3:
                if (goblinRect.left + 120) <= size[0]:
                    if (        (map_array[(enemyCurrentPos[0]+1)][enemyCurrentPos[1]] != 1) \
                            and (map_array[(enemyCurrentPos[0]+1)][enemyCurrentPos[1]] != 2) \
                            and (map_array[(enemyCurrentPos[0]+1)][enemyCurrentPos[1]] != 3) \
                            and (map_array[(enemyCurrentPos[0]+1)][enemyCurrentPos[1]] != 4)):
                        enemyPreviousPos[1] = enemyCurrentPos[1]
                        enemyPreviousPos[0] = enemyCurrentPos[0]
                        enemyCurrentPos[0] += 1
                        goblinRect.left += 60

            map_array[enemyPreviousPos[0]][enemyPreviousPos[1]] = 0
            map_array[enemyCurrentPos[0]][enemyCurrentPos[1]] = 5

            if ((map_array[enemyPreviousPos[0]][enemyPreviousPos[1]] == 0) and \
                    (map_array[enemyPreviousPos[0]][enemyPreviousPos[1]] != 4)):
                MapGen.update((enemyPreviousPos[0]*60),(enemyPreviousPos[1]*60), screen)

            screen.blit(goblin, goblinRect)

