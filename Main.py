# PyGame Classes
import pygame
from pygame.locals import QUIT, KEYDOWN, K_q, K_RIGHT, K_LEFT, K_UP, K_DOWN

# User Classes
from MapGenerator import MapGenerator
from Player import Player
from EnemyController import EnemyController


def main():

    size = WIDTH, HEIGHT = 660, 480
    pygame.init()
    screen = pygame.display.set_mode(size, 0, 8)

    # for the map
    MapGen = MapGenerator()
    map_array = MapGen.generate(screen)
    MapGen.paintMap(map_array, screen)

    player = Player(screen)
    # enemy = Enemy()
    enemies = EnemyController(map_array, screen)
    # enemy.checkPos(map_array)

    # setting screen size and starting pygame
    pygame.display.set_caption('The Purple Pyromancer')

    # The Main Event Loop
    running = True

    while running:
        # Drawing finished this iteration?  Update the screen
        pygame.display.update()
        # player.blitMan(screen)
        # enemy.blitEnemy(screen)

        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
                break
            if event.type == KEYDOWN:
                if (event.key == K_q):
                    running = False
                    break
                if ((event.key == K_RIGHT)
                        or (event.key == K_LEFT)
                        or (event.key == K_UP)
                        or (event.key == K_DOWN)):
                    player.move(map_array, MapGen, screen,
                                size, event.key, enemies)
                    enemies.moveEnemies(map_array, MapGen, screen, size)

    # If game quits, exit.
    pygame.quit()
    print("Exiting!")

    return

if __name__ == "__main__":
    main()
