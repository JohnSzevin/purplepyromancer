import pygame
import random
from pygame.locals import *
from Enemy import Enemy

class EnemyController(pygame.sprite.Sprite):

    # TODO Create an array to take care of enemies, prehaps instantiating them,
    #       and managing them in an array.
    def __init__(self, map_array, screen):
        global enemyArray
        enemyArray = []
        self.createEnemy(map_array, screen)

    def createEnemy(self, map_array, screen):
        global enemy
        enemy = Enemy()
        enemyArray.append(enemy)
        enemy.checkPos(map_array, screen)

    def moveEnemies(self, map_array, MapGen, screen, size):
        if enemyArray:
            for enemy in enemyArray:
                enemy.move(map_array, MapGen, screen, size)

    def killGoblin(self):
        #enemyArray.remove(enemy)
        if enemyArray:
            enemyArray.pop()

